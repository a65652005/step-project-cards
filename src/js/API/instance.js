const token = "b3dcb6c3-4556-4df6-8de6-b87c0ba9442c"
const config = {
    baseURL: 'https://ajax.test-danit.com/api/v2/cards',
    headers: {
        'Content-Type': 'application/json' ,
        'Authorization': `Bearer ${token}`}
}

const instance = axios.create(config);

export default instance;